var express = require('express');
var path = require('path');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var heroRoutes = require('./routes/superheroes');
var villainRoutes = require('./routes/villains');
var app = express();
var port = 3000;

mongoose.connect('mongodb://localhost/superheroes');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));

app.use('/api/superheroes',heroRoutes);
app.use('/api/villains',villainRoutes);

var server = app.listen(port,function(){
  console.log("Listening on port:",port);
});
