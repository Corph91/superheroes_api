var express = require('express');
var Router = express.Router();
var Villain = require('../models/Villain');

Router.route('/')
  .get(function(req,res){
    Villain.find(function(err, villains){
      if(err){
        res.send(err);
      }
      else{
        res.send({message: "Retrieved villains successfully!", data: villains});
      }

    });
  })
  .post(function(req,res){
    var newVillain = new Villain();
    newVillain.name = req.body.name;
    newVillain.evilPower = req.body.evilPower;
    newVillain.img = req.body.img;
    newVillain.save(function(err, newVillain){
      if (err){
        res.send(err);
      }
      else{
        res.json({message: "Villain created!", data: newVillain});
      }

    });
  });

  Router.route('/:_id')
    .get(function(req,res){
      Villain.findById(req.params._id,function(err,villain){
        if(err){
          res.send(err);
        }
        else{
          res.json({message: "Successfully retrieved villain!", data: villain});
        }
      });
    })
    .delete(function(req,res){
      Villain.remove({_id: req.params._id},function(err){
        if(err){
          res.send(err);
        }
        else{
          res.send("Villain deleted!")
        }
      });
    });

module.exports = Router;
